using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoufleCommande : MonoBehaviour
{
    public GameObject Moufle;
    public string axe;

    void Update()
    {
        float input = Input.GetAxis(axe);
        EtatMoufle moveState = EtatTranslationPrInput(input);
        MoufleControleur controller = Moufle.GetComponent<MoufleControleur>();
        controller.translationEtat = moveState;
    }

    EtatMoufle EtatTranslationPrInput(float input)
    {
        if (input > 0)
        {
            return EtatMoufle.Positif;
        }
        else if (input < 0)
        {
            return EtatMoufle.Negatif;
        }
        else
        {
            return EtatMoufle.Fixe;
        }
    }
}
