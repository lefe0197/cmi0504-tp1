using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiedSol : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        // Check if the collision is with an ArticulationBody and if the collided object is a plane (ground)
        if (collision.gameObject.GetComponent<ArticulationBody>() != null && collision.gameObject.CompareTag("Pied"))
        {
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = collision.articulationBody;
        }
    }

    void Update()
    {
        // Check if the 'U' key is pressed and destroy the FixedJoint if it exists
        if (Input.GetKey(KeyCode.U))
        {
            Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    }
}
