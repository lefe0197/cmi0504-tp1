using UnityEngine;

public class ChangerCamera : MonoBehaviour
{
    public Camera MainCamera; // Référence à la première caméra
    public Camera Player; // Référence à la deuxième caméra
    
    private Camera currentCamera; // Référence à la caméra actuelle
    
    // Start is called before the first frame update
    void Start()
    {
        // Définir la première caméra comme la caméra actuelle et la désactiver
        currentCamera = MainCamera;
        Player.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // Si la touche "C" est enfoncée
        if (Input.GetKeyDown(KeyCode.C))
        {
            // Désactiver la caméra actuelle
            currentCamera.gameObject.SetActive(false);
            
            // Changer de caméra
            if (currentCamera == MainCamera)
            {
                currentCamera = Player;
            }
            else
            {
                currentCamera = MainCamera;
            }
            
            // Activer la nouvelle caméra actuelle
            currentCamera.gameObject.SetActive(true);
        }
    }
}
