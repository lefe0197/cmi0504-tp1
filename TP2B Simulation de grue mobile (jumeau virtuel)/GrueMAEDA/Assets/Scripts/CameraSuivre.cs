using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;

// Ce script permet à la caméra de suivre un objet
public class CameraSuivre : MonoBehaviour
{
    public Transform target; // L'objet à suivre
    public Vector3 offset; // Distance entre la caméra et l'objet
    public float smoothSpeed = 0.125f; // Vitesse de suivi

    // LateUpdate est appelé après Update
    void LateUpdate()
    {
        Vector3 desiredPosition = target.position + offset; // Calcule la position désirée de la caméra
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed); // Interpole la position actuelle de la caméra vers la position désirée
        transform.position = smoothedPosition; // Déplace la caméra vers la position désirée
    }
}

