using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportCommande : MonoBehaviour
{
    public GameObject Support;


    void Update()
    {
        float input = Input.GetAxis("Support");
        EtatSupport rotationEtat = MoveStateForInput(input);
        SupportControleur controller = Support.GetComponent<SupportControleur>();
        controller.rotationEtat = rotationEtat;

        
    }

    EtatSupport MoveStateForInput(float input)
    {
        if (input > 0)
        {
            return EtatSupport.Positif;
        }
        else if (input < 0)
        {
            return EtatSupport.Negatif;
        }
        else
        {
            return EtatSupport .Fixe;
        }
    }
}
