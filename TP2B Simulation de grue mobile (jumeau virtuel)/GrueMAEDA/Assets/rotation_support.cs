using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation_support : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.L))
        {
            transform.Rotate(Vector3.forward, 1);
        }
        if (Input.GetKey(KeyCode.M))
        {
            transform.Rotate(Vector3.forward, -1);
        }
    }
}