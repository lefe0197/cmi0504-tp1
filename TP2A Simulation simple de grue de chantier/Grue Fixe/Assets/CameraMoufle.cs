using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform moufle; // L'objet à suivre
    public Vector3 offset; // Le décalage entre la caméra et l'objet

    // Update is called once per frame
    void Update()
    {
        if (moufle != null)
        {
            transform.position = moufle.position;
            // Si vous voulez que la caméra suive également la rotation de l'objet,
            // décommentez la ligne suivante.
             transform.rotation = moufle.rotation;
        }
    }
}