using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declencheur : MonoBehaviour
{
    private int objectCount = 0; // Compteur d'objets
    
    private void OnTriggerEnter(Collider other)
    {
        objectCount++; // Incrémente le compteur d'objets
        Debug.Log("Déclenché par une Charge. Nombre total de charge dans la zone: " + objectCount);
    }

    private void OnTriggerExit(Collider other)
    {
        objectCount--; // Décrémente le compteur d'objets
        Debug.Log("Objet sorti. Nombre total de charge dans la zone: " + objectCount);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
