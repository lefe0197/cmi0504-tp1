using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mouvement : MonoBehaviour
{
    // Déclaration d'une variable pour stocker le composant Rigidbody de l'objet.
    private Rigidbody rb;   

    // La fonction Start est appelée avant la première mise à jour de frame.
    void Start()
    {
        // Récupération du composant Rigidbody de l'objet auquel ce script est attaché.
        rb = GetComponent<Rigidbody>();
    }

    // La fonction Update est appelée une fois par frame.
    void Update()
    {
        // Si la touche flèche du bas est enfoncée...
        if (Input.GetKey(KeyCode.DownArrow))
        {
            // ... et si la touche LeftShift est également enfoncée...
            if (Input.GetKey(KeyCode.LeftShift))
            {
                // ... déplace l'objet vers l'avant à une vitesse accrue.
                transform.Translate(Vector3.forward * 0.10f);
            }
            else
            {
                // ... sinon, déplace l'objet vers l'avant à une vitesse standard.
                transform.Translate(Vector3.forward * 0.05f);
            }
        }
        // Si la touche flèche du haut est enfoncée...
        if (Input.GetKey(KeyCode.UpArrow))
        {
            // ... et si la touche LeftShift est également enfoncée...
            if (Input.GetKey(KeyCode.LeftShift))
            {
                // ... déplace l'objet vers l'arrière à une vitesse accrue.
                transform.Translate(Vector3.back * 0.10f);
            }
            else
            {
                // ... sinon, déplace l'objet vers l'arrière à une vitesse standard.
                transform.Translate(Vector3.back * 0.05f);
            }
        }
        // Si la touche flèche de gauche est enfoncée...
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // ... fait tourner l'objet sur son axe vertical vers la gauche.
            transform.Rotate(Vector3.up, -2);
        }
        // Si la touche flèche de droite est enfoncée...
        if (Input.GetKey(KeyCode.RightArrow))
        {
            // ... fait tourner l'objet sur son axe vertical vers la droite.
            transform.Rotate(Vector3.up, 2);
        }
        // Si la barre d'espace est enfoncée et que l'objet est suffisamment près du sol...
        if (Input.GetKeyDown("space") && transform.position.y <= 1.05f)
        {
            // ... applique une force vers le haut pour faire "sauter" l'objet.
            rb.AddForce(Vector3.up * 3f, ForceMode.Impulse);
        } 
        // Si la touche R est enfoncée...
        if (Input.GetKeyDown(KeyCode.R))
        {
            // ... recharge la scène actuelle.
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }      
    }
}
